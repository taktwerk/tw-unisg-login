<?php

namespace taktwerk\unisglogin;

use linslin\yii2\curl;
use Yii;

class UnisgApi
{
    /**
     * @var \taktwerk\unisglogin\Module UniSG Login module
     */
    public $module;

    private $securityToken;
    private $timestamp;
    private $language;
    private $hash;
    private $entityId;
    private $activeDirectoryId;

    private $technicalSecurityToken;

    /**
     * @var curl\Curl
     */
    protected $curl;

    /**
     * UnisgApi constructor.
     */
    public function __construct()
    {
        $this->curl = new curl\Curl;
    }

    /**
     * Initialise
     * @param $module
     * @param bool $technical
     */
    public function init($module, $technical = false)
    {
        $this->module = $module;

        if ($technical) {
            $postdata = [
                'DefaultRequestHeader'  => array(
                    'Application' => $this->module->applicationId,
                    'RequestedLanguage' => 'de',
                    'RequestedTimeStamp' => substr(date(DATE_ATOM, time()), 0, -6),
                    'SecurityToken' => ''
                ),
                'Password'  => $this->module->technical['password'],
                'UserName'  => $this->module->technical['user']
            ];

            $url    = $this->module->apiUrl . 'V20100101/TechnicalAuthentication/AuthenticateTechnicalUser';

            if ($request = $this->curl->setOption(CURLOPT_POSTFIELDS, http_build_query($postdata))->post($url)) {
                $response   = json_decode($request);

                $securityToken  = $response->Data->DefaultResponseHeader->SecurityToken;
                $this->technicalSecurityToken  = $securityToken;
            }
        }
    }

    /**
     * Check the returned hash for integrity
     *
     * @param $params
     * @return bool
     */
    public function checkHash($params)
    {
        if (!empty($params)) {
            $this->activeDirectoryId = $params['param1'];
            $this->entityId         = $params['param2'];
            $this->timestamp        = $params['param3'];
            $this->language         = $params['param4'];
            $this->hash             = $params['param6'];
            $this->securityToken    = $params['param5'];

            $checkstring =  $this->securityToken . $this->activeDirectoryId . $this->entityId . $this->timestamp . $this->language . $this->module->secretKey;
            $ownhash = md5($checkstring);

            if ($this->securityToken and $this->timestamp and $this->language and ($ownhash === $this->hash)) {
                return true;
            } else {
                /*echo "<b>checkHash</b><br />";
                echo "activeDirectoryID: " . $this->activeDirectoryId . "<br />";
                echo "timestamp: " . $this->timestamp . "<br />";
                echo "language: " . $this->language . "<br />";
                echo "securityToken: " . $this->securityToken . "<br />";
                echo "hash: <b>" . $this->hash . "</b><br />";
                echo "<p>calculated hash: " . $ownhash . "</p>";*/
            }
        }

        return false;
    }

    /**
     * Get or determin the email of the account.
     * @param $identity
     * @return string
     */
    public function getContactMail($identity)
    {
        $mail = $identity->Email;

        // If we don't have an email, ask the API for one
        if (empty($mail)) {
            $masterData = $this->getCommunicationData($identity->HSGEntityId);
            $mail = $this->getContactInfo('Email 1', $masterData->Data);
        }

        // Still no email? Use the HSGentityID
        if (empty($mail)) {
            if (!empty($identity->HSGEntityId)) {
                $mail = $identity->HSGEntityId . '@fake.ch';
            }
        }

        return $mail;
    }

    /**
     * Get the identity
     * @return mixed
     */
    public function getIdentity()
    {
        $url = $this->module->apiUrl . 'V20120101/Identity/GetIdentity?securityToken=' . $this->getSecurityToken() . '&applicationId=' . $this->module->applicationId . '&iso=de';
        //echo "Trying to get identity: $url<br />";

        // Don't check ssl.
        // Todo: remove this security flaw once the uni fixes their ssl certificates!
        $this->curl->setOption(CURLOPT_SSL_VERIFYPEER, false);
        $response = $this->curl->get($url);
        return json_decode($response);
    }

    /**
     * Return all groups of the current user
     * API call
     * @param $hsgID
     * @return array
     */
    public function getOrganisationUnits($hsgID)
    {
        $url = $this->module->apiUrl . 'V20100101/Organisation/GetOrganisationUnitsByHsgEntityPersonId?hsgEntityId=' . $hsgID . '&securityToken=' . $this->getSecurityToken() . '&applicationId=' . $this->module->applicationId . '&iso=de';
        $response = $this->curl->get($url);

        $groups   = json_decode($response);
        if ($groups->Data->DefaultResponseHeader->SecurityToken == $this->getSecurityToken()) {
            return $groups;
        }

        return false;
    }

    /**
     * @return string
     */
    public function getSecurityToken()
    {
        return $this->securityToken;
    }

    /**
     * Get the entity id (param 2)
     * @return mixed
     */
    public function getEntityID()
    {
        return $this->entityId;
    }

    /**
     * @param $type
     * @param $json
     * @return string
     */
    public function getContactInfo($type, $json) {
        $contacts = $json->Contacts;
        if (!empty($contacts)) {
            // find the type and return proper value
            foreach ($contacts as $key => $obj) {
                if ($type === $obj->ContactValueType->ShortNames->German) {
                    return $obj->Data;
                }
            }
        }
        return '';
    }

    /**
     * Gets visible data for certain HSG Entity ID
     * @param $entityID
     * @return array
     */
    public function getDisplayData($entityID)
    {
        $url    = $this->module->apiUrl . 'V20140101/CommunicationData/GetDisplayCommunicationDataForHsgEntityId?hsgEntityId=' . $entityID . '&securityToken=' . $this->technicalSecurityToken . '&applicationId=' . $this->module->applicationId . '&iso=de';
        $response = $this->curl->get($url);
        return json_decode($response);
    }

    /**
     * Gets visible data for certain HSG Entity ID
     * @param $entityID
     * @return array
     */
    public function getCommunicationData($entityID)
    {
        $url = $this->module->apiUrl . 'V20140101/CommunicationData/GetCommunicationDataForHsgEntityId?hsgEntityId=' . $entityID . '&securityToken=' . $this->technicalSecurityToken . '&applicationId=' . $this->module->applicationId . '&iso=de';
        $response = $this->curl->get($url);
        return json_decode($response);
    }

    /**
     * Gets visible data for certain HSG Entity ID
     * @param $entityID
     * @return array
     */
    public function getAlexandriaName($entityID)
    {
        $url = $this->module->apiUrl . 'V20140101/PersonProfile/GetPublicationNameAlexandria?hsgEntityId=' . $entityID . '&securityToken=' . $this->technicalSecurityToken . '&applicationId=' . $this->module->applicationId . '&iso=de';
        $response = $this->curl->get($url);
        return json_decode($response);
    }

    /**
     * Get HSG Personprofile passphoto
     * @param $entityID
     * @return mixed
     */
    public function getPassPhoto($entityID)
    {
        $url = $this->module->apiUrl . 'V20140101/PersonProfile/GetPassPhotoByHsgEntityId?hsgEntityId=' . $entityID . '&securityToken=' . $this->technicalSecurityToken . '&applicationId=' . $this->module->applicationId . '&iso=de';
        $response = $this->curl->get($url);
        return json_decode($response);
    }

    /**
     * get title prefix from person manager
     * @param $entityID
     * @return mixed
     */
    public function getTitlePrefix($entityID)
    {
        $url = $this->module->apiUrl . 'V20140101/PersonProfile/GetCurrentPrefixTitleForHsgEntityId?hsgEntityId=' . $entityID . '&securityToken=' . $this->technicalSecurityToken . '&applicationId=' . $this->module->applicationId . '&iso=de';
        $response = $this->curl->get($url);
        return json_decode($response);
    }

    /**
     * delivers all profile texts from the api
     * @param $entityID
     * @return mixed
     */
    public function getProfileTexts($entityID)
    {
        $url = $this->module->apiUrl . 'V20140101/PersonProfile/GetPersonProfileTexts?personHsgEntityId=' . $entityID . '&securityToken=' . $this->technicalSecurityToken . '&applicationId=' . $this->module->applicationId;
        $response = $this->curl->get($url);
        return json_decode($response);
    }
}
