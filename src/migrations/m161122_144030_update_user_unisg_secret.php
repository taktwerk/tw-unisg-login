<?php

use yii\db\Migration;
use yii\db\Schema;

class m161122_144030_update_user_unisg_secret extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%user}}', 'unisg_secret_key', Schema::TYPE_TEXT);
    }

    public function down()
    {
        $this->alterColumn('{{%user}}', 'unisg_secret_key', Schema::TYPE_STRING . "(255)");
    }
}
