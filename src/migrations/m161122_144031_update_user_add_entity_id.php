<?php

use yii\db\Migration;
use yii\db\Schema;

class m161122_144031_update_user_add_entity_id extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'unisg_entity_id', Schema::TYPE_STRING . "(255) NULL");
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'unisg_entity_id');
    }
}
