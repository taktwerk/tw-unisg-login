<?php

use yii\db\Migration;
use yii\db\Schema;

class m161122_144029_update_user_unisg_login extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'is_unisg', Schema::TYPE_BOOLEAN . "(1) NULL DEFAULT 0");
        $this->addColumn('{{%user}}', 'unisg_secret_key', Schema::TYPE_STRING . "(255)");
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'is_unisg');
        $this->dropColumn('{{%user}}', 'unisg_secret_key');
    }
}
