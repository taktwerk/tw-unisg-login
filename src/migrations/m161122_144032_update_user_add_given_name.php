<?php

use yii\db\Migration;
use yii\db\Schema;

class m161122_144032_update_user_add_given_name extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'unisg_name', Schema::TYPE_STRING . "(255) NULL");
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'unisg_name');
    }
}
