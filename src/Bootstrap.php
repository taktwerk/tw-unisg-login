<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace taktwerk\unisglogin;

use Yii;
use yii\authclient\Collection;
use yii\base\BootstrapInterface;
use yii\base\Application;
use yii\console\Application as ConsoleApplication;
use yii\i18n\PhpMessageSource;

/**
 * Bootstrap registers stuff to the yii framework.
 *
 * Class Bootstrap
 * @package taktwerk\unisglogin
 */
class Bootstrap implements BootstrapInterface
{
    /**
     * Bootstrap process
     * @param $app
     */
    public function bootstrap($app)
    {
        // Before the rest, we want to set up some aliases
        $this->registerAliases();

        if ($app->hasModule('unisglogin') && ($module = $app->getModule('unisglogin')) instanceof Module) {
            if ($app instanceof ConsoleApplication) {
                // Do something special when in console mode?
            } else {
                $configUrlRule = [
                    'prefix' => $module->urlPrefix,
                    'rules' => $module->urlRules,
                ];

                // Force the route Prefix to unisglogin to match the name of the module, but the route can be
                // unisg-login or anything else the user wants to set up.
                if ($module->urlPrefix != 'unisglogin') {
                    $configUrlRule['routePrefix'] = 'unisglogin';
                }

                $configUrlRule['class'] = 'yii\web\GroupUrlRule';
                $rule = Yii::createObject($configUrlRule);

                $app->urlManager->addRules([$rule], false);
            }
        }

        // Register migrations
        $this->registerMigration($app);
    }

    /**
     * Register the package migrations
     * @param Application $app
     */
    protected function registerMigration(Application $app)
    {
        $app->params['yii.migrations'][] = '@taktwerk-unisglogin/migrations';
    }

    /**
     * Register some aliases
     */
    protected function registerAliases()
    {
        Yii::setAlias('@taktwerk-unisglogin', '@vendor/taktwerk/tw-unisg-login/src');
    }
}
