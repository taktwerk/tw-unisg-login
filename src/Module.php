<?php


namespace taktwerk\unisglogin;

use yii\base\Module as BaseModule;
use Yii;

/**
 * This is the main module class for the Uni SG Login module.
 *
 * Initialisation example:
 *
 * Simple example:
 *
 * ~~~
 * 'modules' => [
 *     'unisglogin' => [
 *         'class' => 'taktwerk\unisglogin\Module',
 *     ],
 * ],
 * ~~~
 *
 * Complex example:
 *
 * ~~~
 * 'modules' => [
 *     'unisglogin' => [
 *         'class' => 'taktwerk\unisglogin\Module',
 *         'secretKey' => 'secretKey',    // The secret key for the API.
 *         'applicationId' => 'applicationId', // The application API id
 *         'apiUrl' => 'http://apiUrl/', // The API url with trailing dash
 *         'technical' => [
 *              'user' => 'user', // The technical user for login
 *              'password' => 'password'
 *          ],
 *          'urlPrefix' => 'unisglogin',
 *          'urlRules' => [
 *              'callback' => 'auth/callback',
 *              'login' => 'auth/login',
 *              'logout' => 'auth/logout'
 *          ],
 *     ],
 * ],
 */
class Module extends BaseModule
{
    /**
     * Version of the module
     */
    const VERSION = '0.1.1';

    /**
     * @var string Url Prefix for callbacks
     */
    public $urlPrefix = 'unisg-login';

    /**
     * @var array
     */
    public $urlRules = [
        'callback' => 'auth/callback',
        'login' => 'auth/login',
        'logout' => 'auth/logout'
    ];

    /**
     * @var string Secret Key for the API login process
     */
    public $secretKey = '';

    /**
     * @var string Application ID for the login process
     */
    public $applicationId = '';

    /**
     * @var string API Url for the login
     */
    public $apiUrl = '';

    /**
     * @var array Technical user information for the timetable process
     */
    public $technical = [
        'user' => '',
        'password' => '',
    ];

    /**
     * @var string Encryption key for the AES process (voting tool)
     */
    public $aesSecret = '';
}
