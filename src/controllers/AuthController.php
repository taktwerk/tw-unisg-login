<?php

namespace taktwerk\unisglogin\controllers;

use dektrium\user\helpers\Password;
use taktwerk\unisglogin\UnisgApi;
use taktwerk\unisglogin\Module;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use dektrium\user\models\User;
use dektrium\user\traits\EventTrait;
use taktwerk\yiiboilerplate\helpers\DebugHelper;

/**
 * Class AuthController
 * @package taktwerk\unisg\login\controllers
 */
class AuthController extends Controller
{
    use EventTrait;

    /**
     * @var bool debug echoing
     */
    protected $debug = false;

    /**
     * @var UnisgApi
     */
    protected $unisgApi;

    /**
     * @var object of the user
     */
    protected $identity;

    /**
     * @var Module
     */
    public $module;

    /**
     * Callback from the form with valid login data
     */
    public function actionCallback()
    {
        $params = Yii::$app->request->getQueryParams();

        if (!empty($params['debug'])) {
            $this->debug = $params['debug'] == 'true';
        }

        // Validate the hash of the request
        $this->unisgApi = new UnisgApi();
        $this->unisgApi->init($this->module);
        if (!$this->unisgApi->checkHash($params)) {
            $this->error(Yii::t('app', 'Unsuccessfull hash'));
        }

        $retry = true;
        $tries = 0;
        while ($retry) {
            $retry = false;
            $tries++;
            $response = $this->unisgApi->getIdentity();

            // Validate the security token sent
            if ($response->Data->DefaultResponseHeader->SecurityToken == $this->unisgApi->getSecurityToken()) {
                $this->log("Security token matches");
                $this->identity = $response->Data->Identity;
                $hsg = $response->Data->Identity->HSGEntityId;
                $mail = $this->unisgApi->getContactMail($this->identity);

                $this->log("Identity", $this->identity);
                $this->log('hsg', $hsg);
                $this->log('mail', $mail);

                if (empty($mail)) {
                    $this->error(Yii::t('app', 'Couldn\'t find a valid email for this the user. Please try again later.'));
                }

                // If it took more than one try, log it in sentry for this email.
                if ($tries > 1) {
                    Yii::error('SHSG API needed ' . $tries . ' tries for email ' . $mail);
                }

                // Log in the sent user
                $this->logUser($mail);
            } elseif (empty($response)) {
                // If we didn't get a response, retry in a second up to 9 times
                if ($tries < 10) {
                    sleep(1);
                    $retry = true;
                } else {
                    // No longer retrying, still raise the error
                    $this->error(Yii::t('app', "The SHSG API didn't send any information back"));
                }
            } else {
                $this->error(Yii::t('app', "Security token didn't match."));
            }
        }
    }

    /**
     *
     */
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        // Redirect to login page
        $urls = $this->module->urlRules;

        $url = getenv('UNISG_LOGIN_URL');
        $url .= '?lang=' . Yii::$app->language;
        $url .= '&returnUrl=' .  Yii::$app->urlManager->createAbsoluteUrl($urls['callback']);
        $url .= '&returnCancelUrl=' . Yii::$app->urlManager->createAbsoluteUrl('');
        $url .= '&referrer=' . $this->getReferrer();
        return $this->redirect($url);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $event = $this->getUserEvent(\Yii::$app->user->identity);

        $this->trigger('beforeLogout', $event);

        \Yii::$app->getUser()->logout();

        $this->trigger('afterLogout', $event);

        return $this->goHome();
    }

    /**
     * Log in a user
     *
     * @param $email
     * @return mixed
     */
    protected function logUser($email)
    {
        // Check if the user exists using the unisg id (if it's not 00000000)
        $identity = $this->unisgApi->getEntityID() != '00000000' ? User::findOne(['unisg_entity_id' => $this->unisgApi->getEntityID()]) : null;
        if (empty($identity)) {
            $identity = User::findOne(['email' => $email]);
        }

        if (empty($identity)) {
            // Set up the account
            $identity = $this->setupAccount($email);
        }

        // Identity still null? Obviously we have an error and need to check what it is.
        if (empty($identity)) {
            $this->error(Yii::t('app', "Error: Couldn't set up or connect account identity. Aborting."), 0, true);
        }

        // Log in the user
        Yii::$app->getUser()->login($identity);

        // Save security key for API and Sync calls
        $identity->unisg_secret_key = $this->unisgApi->getSecurityToken();
        $identity->unisg_entity_id = $this->unisgApi->getEntityID();
        $identity->email = $email; // Save new email (when they change from @unisg.ch to @student.unisg.ch)
        $identity->unisg_name = ucfirst(strtolower($this->identity->FirstName));
        $identity->is_unisg = true; // Some older accounts were set up without the is_unisg to true.
        $identity->save();

        $referrer = $this->getCallbackReferrer();

        return $this->redirect($referrer);
    }

    /**
     * Set up a new account
     *
     * @param $email
     * @return mixed
     */
    protected function setupAccount($email)
    {
        $this->log("Set up account for", $email);
        // Account identity
        $fullname = strtolower($this->identity->FirstName . ' ' . $this->identity->LastName);
        $username = $this->identity->HSGEntityId;
        $user = Yii::createObject([
            'class'    => User::className(),
            'scenario' => 'connect',
            'username' => $username,
            'email'    => $email,
            'fullname' => $fullname,
            'password_hash' => Password::generate(6), // generate a random hash. User can't log in anyway.
            'auth_key' => 'fake', // Is replaced in the beforeSave method.
            'is_unisg' => true,
        ]);

        $this->log("Username", $username);


        if ($user->create()) {
            $this->log("Account created");
            return User::findOne(['email' => $email]);
        } else {
            $this->log("Couldn't create account!");
            $this->error('Couldn\'t create account: ' . print_r($user->getErrors()), 0, true);
        }
        return null;
    }

    /**
     * Get the referrer of the current page
     * @return string
     */
    protected function getReferrer()
    {
        $referrer = Yii::$app->request->get('referrer');
        if (!empty($referrer)) {
            return $referrer;
        }
        return Url::previous();
    }

    /**
     * Get the referrer parameter from the callback page
     */
    protected function getCallbackReferrer()
    {
        $referrer = Yii::$app->request->getReferrer();

        // Analyse the url and extract the parameters
        $query = parse_url($referrer, PHP_URL_QUERY);
        parse_str($query, $params);

        if (!empty($params['referrer'])) {
            return $params['referrer'];
        }
        return '/';
    }

    /**
     * Quick debugging logger
     * @param $message
     * @param null $params
     * @return null
     */
    protected function log($message, $params = null)
    {
        if (!$this->debug) {
            return null;
        }

        echo "<p>$message";
        if (is_array($params) || is_object($params)) {
            echo ":<br />";
            DebugHelper::pre($params);
        } elseif ($params !== null) {
            echo ": $params";
        }
        echo "</p>";
    }

    /**
     * Throw an exception
     * @param $message
     * @param int $code
     * @throws \Exception
     */
    protected function error($message, $code = 0, $addIdentity = false)
    {
        if ($addIdentity) {
            $message .= ' Identity: ' . json_encode($this->identity);
        }
        throw new \Exception('UniSG Login: ' . $message, $code);
    }
}
