<?php

namespace taktwerk\unisglogin;

use Yii;

/**
 * Initialisation of the unisg login tool.
 * 
 * Initialisation example:
 * 
 * ~~~
 * 'bootstrap' => ['unisglogin'],
 * 'component' => [
 *      'unisglogin' => [
 *          'class' => 'taktwerk\unisglogin\Component'
 *      ]
 * ]
 */
class Component extends \yii\base\Component {

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();
    }
}
