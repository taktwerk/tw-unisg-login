tw-unisg-login
============

Module to add support for unisg login.

Installation 
------------

Add the following in your composer.json file:

```json
"require": {
    "taktwerk/tw-unisg-login": "*"
},
"repositories": [
    {
        "type": "git",
        "url": "https://bitbucket.org/taktwerk/tw-unisg-login.git",
        "reference": "package"
    }
]
```

Add the following in your modules config:

```php
'bootstrap' => [
    'taktwerk\unisglogin\Bootstrap'
],
'modules' => [
    'unisglogin' => [
        'class' => 'taktwerk\unisglogin\Module',
        'secretKey' => 'secretKey',    // The secret key for the API.
        'applicationId' => 'applicationId', // The application API id
        'apiUrl' => 'apiUrl', // The API url
        'technical' => [
             'user' => 'user', // The technical user for login
             'password' => 'password'
         ],
         'urlPrefix' => 'unisg-login', // Prefix before the callback action
         'urlRules' => [
            'callback' => 'auth/callback' // Change the array key to change the url name.
         ]
    ],
],
```
This will add the route `unisg-login/callback`, but this can be changed in the config.

### env
Add a `UNISG_LOGIN_URL` parameter in your `.env` file. This is the url called when wanting to log in on auth/login.